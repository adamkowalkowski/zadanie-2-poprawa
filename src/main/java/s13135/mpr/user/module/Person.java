package s13135.mpr.user.module;

import java.util.List;

public class Person {

	private int id;
	private String name;
	private String surname;
	private user user;
	private int idAddress;
	private List<Adres> addresses;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public user getUser() {
		return user;
	}

	public void setUser(user user) {
		this.user = user;
	}

	public List<Adres> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Adres> addresses) {
		this.addresses = addresses;
	}

	public int getIdAddress() {
		return idAddress;
	}

	public void setIdAddress(int idAddress) {
		this.idAddress = idAddress;
	}
	
}
